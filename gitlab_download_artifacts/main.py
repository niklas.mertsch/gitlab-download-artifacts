from __future__ import annotations

import argparse
from io import BytesIO
from typing import Optional, BinaryIO
import zipfile

from gitlab import Gitlab
from gitlab.v4.objects import Project, ProjectJob


def parse_args(args: Optional[list[str]] = None):
    parser = argparse.ArgumentParser(description='Download artifacts of latest successful job')
    parser.add_argument('-p', '--project-path', help='project path (including group)')
    parser.add_argument('-a', '--artifact-file', help='name of the artifact file to download (default: all)',
                        default=None)
    parser.add_argument('-t', '--token', help='GitLab project access token', default=None)
    parser.add_argument('-o', '--out_directory', default='.', help='output location (default: .)')
    parser.add_argument('-j', '--job-name', default='build', help='name of the job producing the artifact '
                                                                  '(default: build)')
    parser.add_argument('-b', '--branch', default='master', help='name of the branch (default: master)')
    parser.add_argument('-i', '--instance', default='https://gitlab.gwdg.de', help='url of the GitLab instance '
                                                                                   '(default: https://gitlab.gwdg.de)')
    return parser.parse_args(args)


def get_project(gitlab: Gitlab, project_path: str) -> Project:
    project_path = project_path.lower()
    *group_paths, project_name = project_path.split("/")
    projects_by_path = {project.path_with_namespace.lower(): project for project in gitlab.projects.list()}
    if project_path in projects_by_path:
        return gitlab.projects.get(projects_by_path[project_path].id)

    groups_by_path = {group.path.lower(): group for group in gitlab.groups.list()}
    if group_paths:
        for group_path in group_paths:
            if group_path not in groups_by_path:
                raise ValueError(f"Group {group_path} not found")
            group = groups_by_path[group_path]
            groups_by_path = {group.path.lower(): group for group in group.subgroups.list()}

        group_projects = {project.path.lower(): project for project in group.projects.list()}
        if project_name not in group_projects:
            raise ValueError(f"Project {project_name} not found")

        project = group_projects[project_name]
        return gitlab.projects.get(project.id)

    raise ValueError(f"Project {project_path} not found")


def get_job(project: Project, branch: str, job_name: str) -> ProjectJob:
    pipeline = max((pipeline for pipeline in project.pipelines.list()
                    if pipeline.status == 'success' and pipeline.ref == branch),
                   key=lambda p: p.created_at)
    for job in pipeline.jobs.list():
        job = project.jobs.get(job.id)
        if job.name == job_name:
            return job
    else:
        raise RuntimeError("No suitable job found")


def download_artifact(job: ProjectJob, artifact_filename: Optional[str] = None) -> tuple[str, BytesIO]:
    zip_file = BytesIO()
    try:
        if artifact_filename is None:
            job.artifacts(streamed=True, action=zip_file.write)
            filename = job.artifacts_file["filename"]
        else:
            job.artifact(artifact_filename, streamed=True, action=zip_file.write)
            filename = artifact_filename
    except:
        raise RuntimeError(f'Failed to download {artifact_filename}')

    return filename, zip_file


def unzip_file(zip_file: BinaryIO, target_dir: str) -> None:
    with zipfile.ZipFile(zip_file) as f:
        f.extractall(path=target_dir)


def main():
    args = parse_args()

    # open connection
    with Gitlab(args.instance, private_token=args.token) as gl:
        project = get_project(gl, args.project_path)
        job = get_job(project, branch=args.branch, job_name=args.job_name)
        filename, zip_file = download_artifact(job, args.artifact_file)
        unzip_file(zip_file, args.out_directory)
