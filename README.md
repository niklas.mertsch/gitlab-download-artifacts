# Download GitLab Artifacts
- Requires Python >= 3.7
- Installation: `pip install git+https://gitlab.gwdg.de/niklas.mertsch/gitlab-download-artifacts`
- How to run: `gitlab-download-artifacts [OPTIONS]`

### Options:
```
$ gitlab-download-artifacts --help
usage: gitlab-download-artifacts [-h] [-p PROJECT_PATH] [-a ARTIFACT_FILE] [-t TOKEN] [-o OUT_DIRECTORY] [-j JOB_NAME] [-b BRANCH] [-i INSTANCE]

Download artifacts of latest successful job

optional arguments:
  -h, --help            show this help message and exit
  -p PROJECT_PATH, --project-path PROJECT_PATH
                        project path (including group)
  -a ARTIFACT_FILE, --artifact-file ARTIFACT_FILE
                        name of the artifact file to download (default: all)
  -t TOKEN, --token TOKEN
                        GitLab project access token
  -o OUT_DIRECTORY, --out_directory OUT_DIRECTORY
                        output location (default: .)
  -j JOB_NAME, --job-name JOB_NAME
                        name of the job producing the artifact (default: build)
  -b BRANCH, --branch BRANCH
                        name of the branch (default: master)
  -i INSTANCE, --instance INSTANCE
                        url of the GitLab instance (default: https://gitlab.gwdg.de)
```
