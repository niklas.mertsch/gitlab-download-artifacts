from setuptools import setup, find_packages
setup(
    name='gitlab-download-artifacts',
    version='1.0',
    description='Download the latest artifacts from a GitLab project',
    url='https://gitlab.com/sila2',
    author='Niklas Mertsch',
    author_email='niklas.mertsch@stud.uni-goettingen.de',
    python_requires='>=3.7',
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "gitlab-download-artifacts=gitlab_download_artifacts.main:main",
        ],
    },
    install_requires=[
        "python-gitlab"
    ],
    zip_safe=False
)
